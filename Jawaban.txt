1. Buat Database

 create database myshop;


2. Membuat Table Didalam Database

use myshop;
create table users (id intauto_increment primary key, name varchar (225), email varchar (225), password varchar (225));

create table categories (id int auto_increment primary key, name varchar (225));

create table users (id int auto_increment primary key, name varchar (225), description varchar (225); price int, stock int, category_id int, foreign key (category_id) references categories (id));


3. masukan data pada tabel

insert into users (name,email,password)
    -> values ("John Doe","john@doe.com","john123"),
    -> ("John Doe","john@doe.com","john123");

insert into categories (name)
    -> values ("gadget"),
    -> ("cloth"),
    -> ("men"),
    -> ("women"),
    -> ("branded");

insert into items(name,description,price,stock,category_id)
    -> values("Samsung b50","hape keren dari merek samsung",4000000,100,1),
    -> ("Uniklooh","baju keren dari brand ternama",500000,50,2),
    -> ("IMHO Watch","jam tangan anak yang jujur banget",2000000,10,1);

4. mengambil data dari database
   a. mengambil data users

 select id,name,email from users;

   b. mengambil data items
 select * from items where price > 1000000;

 select * from items where name like "%uniklo%";

 select items.name,items.description,items.price,items.stock,items.category_id, categories.name as kategori from items inner join categories on items.category_id=categories.id;


5.mengubah data dari database

update items set price  = 2500000 where name = "Samsung b50"; 




